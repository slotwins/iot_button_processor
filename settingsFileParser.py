from cryptography.fernet import Fernet
from configparser import ConfigParser, RawConfigParser
from dotenv import set_key, get_key
from os import path


# Get variable from INI File
def getVariable(iniFileLocation, sectionName, varName, isEncrypted=False, encryptKeyName=None):
    varName = varName.lower()
    config = RawConfigParser()
    config.read(iniFileLocation)
    if sectionName in config.sections():
        # create list of variable names from the key value pair stored in the items
        varList = [var[0] for var in config.items(sectionName)]
        if varName in varList:
            if isEncrypted:
                return decryptValue(encryptKeyName, config.get(sectionName, varName))
            else:
                return config.get(sectionName, varName)
        else:
            print('Specified variable name %s not found in section %s' % (varName, sectionName))
            if isEncrypted:
                return decryptValue(encryptKeyName,
                                    putVariable(iniFileLocation=iniFileLocation, sectionName=sectionName,
                                                varName=varName, isEncrypted=isEncrypted,
                                                encryptKeyName=encryptKeyName))
            else:
                return putVariable(iniFileLocation, sectionName, varName)

    else:
        print('Specified section name %s NOT found for variable %s' % (sectionName, varName))
        if isEncrypted:
            return decryptValue(encryptKeyName, putVariable(iniFileLocation=iniFileLocation, sectionName=sectionName,
                                                            varName=varName, isEncrypted=isEncrypted,
                                                            encryptKeyName=encryptKeyName))
        else:
            return putVariable(iniFileLocation, sectionName, varName)


# Put variable to INI File
def putVariable(iniFileLocation, sectionName, varName, varValue=None, isEncrypted=False, encryptKeyName=None):
    varName = varName.lower()
    config = RawConfigParser()
    config.read(iniFileLocation)
    if sectionName not in config.sections():
        config.add_section(sectionName)
    while varValue is None:
        varValue = input('Enter a value for variable %s:  ' % varName)
    if isEncrypted:
        varValue = encryptValue(encryptKeyName, varValue)
    config.set(sectionName, varName, varValue)
    with open(iniFileLocation, 'w') as configfile:  # save
        config.write(configfile)
    print('Variable %s written to Section %s in config file %s' % (varName, sectionName, iniFileLocation))
    return varValue


# encrypt
def encryptValue(keyName, varValue):
    key = __getSetKey__(keyName)
    f = Fernet(key)
    encrypt_value = f.encrypt(varValue.encode())
    return encrypt_value.decode()


# decrypt
def decryptValue(keyName, varValue):
    key = __getSetKey__(keyName)
    f = Fernet(key)
    decrypted_value = f.decrypt(varValue.encode())
    return decrypted_value.decode()


# get/set encryption key from environment - THIS FUNCTION IS NOT SHARED
def __getSetKey__(keyName):
    keyFile = '.info'
    if not path.exists(keyFile):
        open(keyFile, 'a').close()
    key = get_key(keyFile, keyName)
    if key is None:
        print('!!!ENCRYPTION KEY NOT FOUND!!!\r\n'
              'A NEW KEY HAS BEEN SET AND STORED\r\n'
              'EXISTING ENCRYPTED PASSWORDS WILL NEED TO BE RESET\r\n')
        key = Fernet.generate_key()
        set_key(keyFile, keyName, key.decode())
    return key


appName = 'DoorChecker'  # used for keyName and section


# print(encryptValue('test', 'this is a test value'))
# print(decryptValue('test', 'gAAAAABe20U0gZ_NYiGpPl2sx6HW54tEbo8vrQEgpyxrquyvznHqJUmV-kLsDfFb_JMECI-DcOLY1fHseL25d_xPMkvvOjJKHk6si7omAQ95al7IV98x9ek=' ))
# print(getVariable('test.ini', 'Testing123', 'testVar4', True, appName))
