from settingsFileParser import getVariable
from kafka import KafkaConsumer
from json import loads
import pymysql
from sys import exc_info
import sendTelegramMessage
import trelloPostCard
import socket
import traceback


def MessageProcessor():
    # Settings File Variables
    kafkaServer = getVariable(iniFileLocation, appName, 'kafkaServer', True, appName)
    kafkaGroup = getVariable(iniFileLocation, appName, 'kafkaGroup', True, appName)
    kafkaTopic = getVariable(iniFileLocation, appName, 'kafkaTopic', True, appName)
    file_caCert = getVariable(iniFileLocation, appName, 'file_caCert', True, appName)
    file_clientCert = getVariable(iniFileLocation, appName, 'file_clientCert', True, appName)
    file_clientKey = getVariable(iniFileLocation, appName, 'file_clientKey', True, appName)
    certPassword = getVariable(iniFileLocation, appName, 'certPassword', True, appName)
    dbHost = getVariable(iniFileLocation, appName, 'dbHost', True, appName)
    dbUser = getVariable(iniFileLocation, appName, 'dbUser', True, appName)
    dbPasswd = getVariable(iniFileLocation, appName, 'dbPasswd', True, appName)
    dbAppSchema = getVariable(iniFileLocation, appName, 'dbAppSchema', True, appName)
    db = pymysql.connect(dbHost, dbUser, dbPasswd, dbAppSchema)

    consumer = KafkaConsumer(
        kafkaTopic,
        bootstrap_servers=[kafkaServer],
        security_protocol='SSL',
        ssl_cafile=file_caCert,
        ssl_certfile=file_clientCert,
        ssl_keyfile=file_clientKey,
        ssl_password=certPassword,
        ssl_check_hostname=False,
        auto_offset_reset='latest',
        enable_auto_commit=True,
        group_id=kafkaGroup,
        value_deserializer=lambda x: loads(x.decode('utf-8')))

    print('Consumer Starter... Waiting...')
    for message in consumer:
        message = message.value
        print('1. Process Message')
        print(message)
        # get notifications for the sensor
        notificationList = getButtonMetadata(db, dbAppSchema, 'notificationType, recipients', 'iot_buttonNotifications',
                                             "SensorID=" + str(message['sensorID']) + " AND enabled = 1")
        print('2. Process Notifications')
        for notification in notificationList:
            print(notification)
            if notification[0] == 'telegram': sendTelegramMessage.sendTelegram(iniFileLocation, message['message'], notification[1])
            if notification[0] == 'trello': trelloPostCard.trelloPostCard(iniFileLocation, message['message'], message['eventDBid'], notification[1])
            if notification[0] == 'test': print('test output')
    consumer.commit()


def getButtonMetadata(db, schema, selectFields, fromTable, predicateString):
    db.ping(reconnect=True)
    cursor = db.cursor()  # prepare a cursor object using cursor() method
    try:
        selectSQL = "SELECT %s \
                    FROM %s.%s \
                    WHERE true \
                    AND %s" % (selectFields, schema, fromTable, predicateString)
        cursor.execute(selectSQL)
        dbDataSet = cursor.fetchall()
        if len(dbDataSet) == 0: raise Exception("NO RESULTS -- ERROR DURING BUTTON METADATA QUERY")
        # return the Sensor ID of the device
        return dbDataSet
    except:
        # Rollback in case there is any error
        db.rollback()
        db.close()
        e = exc_info()
        print("!!! DATABASE ERROR ENCOUNTERED: " + str(e))
        exit(99)


if __name__ == "__main__":
    print('STARTING BUTTON NOTIFICATIONS PROCESSOR')
    # Primary Variables
    appName = 'Button_Notifications_Processor'
    iniFileLocation = 'settings_Message_Processors.ini'
    telegramSystemsChatID = getVariable(iniFileLocation, appName, 'telegramSystemsChatID', True, appName)
    try:
        MessageProcessor()
    except KeyboardInterrupt:
        exit(0)
    except:
        print('\r\n\r\n!!!ERROR DETECTED!!!\r\n')
        # # ERROR HANDLING TO TELEGRAM HERE
        errorMessage = "ERROR IN IOT BUTTON NOTIFICATION PROCESSOR ON : " + socket.gethostname() + \
                       "\r\n\r\n" + str(traceback.format_exc())
        print(errorMessage)
        sendTelegramMessage.sendTelegram(iniFileLocation, errorMessage, telegramSystemsChatID)
        exit(97)
