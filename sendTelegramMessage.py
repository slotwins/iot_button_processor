import requests
from settingsFileParser import getVariable


def sendTelegram(iniFileLocation, bot_message, bot_chatID):
    appName = 'Button_Notifications_Telegram'
    bot_token = getVariable(iniFileLocation, appName, 'bot_token', True, appName)
    print('--- Sending Telegram to chat %s' % bot_chatID)
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    print('--- Status Code: %s' % response.status_code)
    return response.status_code


if __name__ == "__main__":
    test = sendTelegram('Testing')
    print(test)
