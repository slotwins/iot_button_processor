#!/bin/sh
# ------------------------------------------

# Set the environment
# export LD_LIBRARY_PATH=<...>:$LD_LIBRARY_PATH
export PYTHONPATH=/opt/pythonHome/.local/lib/python2.7/site-packages/dotenv/,/opt/pythonHome/.local/bin:$PYTHONPATH

D=`dirname $0`
exec python3 $D/kafka_buttonNotificationsProcessor.py
