import requests
import json
import os
import datetime
from settingsFileParser import getVariable


def trelloPostCard(iniFileLocation, name, desc, boardList):
    print('... Starting Trello Card Create ...')
    appName = 'Button_Notifications_Trello'
    apiKey = getVariable(iniFileLocation, appName, 'trelloKey', True, appName)
    apiToken = getVariable(iniFileLocation, appName, 'trelloToken', True, appName)
    boardName = boardList.split('/')[0]
    listName = boardList.split('/')[1]
    assignToName = boardList.split('/')[2]
    #####
    apiBoardID = 'NONE'
    apiListID = 'NONE'
    assignToID = 'NONE'
    # base parameter dictionary
    baseParameterList = {'key': apiKey,'token': apiToken}
    # Get a the ID for the board name specified
    boardsParamList = baseParameterList
    boardsParamList['fields'] = 'name'
    boardsList = trelloAPIget('GET', 'members/me/boards', boardsParamList)
    for board in boardsList:
        if board['name'] == boardName: apiBoardID = board['id']
    print('BOARD ID : ' + apiBoardID)
    # Get the ID of the list parameter specifies
    listList = trelloAPIget('GET', 'boards', baseParameterList, apiBoardID, 'lists')
    for item in listList:
        if item['name'] == listName: apiListID = item['id']
    print('LIST ID : ' + apiListID)
    # Get the Member ID of the person to assign the card to
    memberParamList = baseParameterList
    memberParamList['fields'] = 'username'
    memberList = trelloAPIget('GET', 'boards', memberParamList, apiBoardID, 'members')
    for member in memberList:
        if member['username'] == assignToName: assignToID = member['id']
    print('MEMBER ID FOR NOTIFICATION : ' + assignToID)

    # Create the card in the list
    cardParamList = baseParameterList
    cardParamList['idList'] = apiListID
    cardParamList['name'] = name
    cardParamList['desc'] = desc
    cardParamList['idMembers'] = assignToID
    cardParamList['pos'] = 'top'
    cardParamList['due'] = str(datetime.datetime.utcnow())
    cardInserted = trelloAPIget('POST', 'cards', cardParamList)
    print('ADDED CARD WITH ID ' + cardInserted['id'])
    return cardInserted['id']


def trelloAPIget(operation, module, parameterList, moduleID=None, subModule=None):
    print('...API %s Call...' % operation)
    url = "https://api.trello.com/1/" + module + "/?"
    if moduleID is not None and subModule is not None: url = "https://api.trello.com/1/" + module + "/" + moduleID + "/" + subModule + "/?"
    headers = {"Accept": "application/json"}
    response = requests.request(
        operation,
        url,
        headers=headers,
        params=parameterList
    )
    return json.loads(response.text)


if __name__ == "__main__":
   trelloPostCard('Python Test2', '233', 'Test/queue/lukeslotwinski')

